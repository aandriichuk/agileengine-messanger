﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using AgileEngine.DataLayer.Common;
using AgileEngine.DataLayer.Common.Models;

namespace AgileEngine.Messanger.MicroService.Controllers
{
    [Route("api/[controller]")]
    public class SendMessageController
    {
        private readonly IMessagingService _messagingService;

        public SendMessageController(IMessagingService messagingService)
        {
            _messagingService = messagingService;
        }

        // POST api/sendmessage

        [HttpPost]
        public Guid Post([FromBody]SendMessageRequest request)
        {
            var messageId = _messagingService.SendAsync(request.Text, request.Recipients);

            return messageId;
        }
    }
}
