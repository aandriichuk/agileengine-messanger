﻿using AgileEngine.Messanger.CommandStack.Commands;
using AgileEngine.Messanger.Infrastructure.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.MicroService
{
    public class MessagingService : IMessagingService
    {
        IHandleMessage<SendMessageCommand> _sendMessageHandler;

        public MessagingService(IHandleMessage<SendMessageCommand> sendMessageHandler)
        {
            _sendMessageHandler = sendMessageHandler;
        }

        public Guid Send(string text, string recipients)
        {
            var messageId = Guid.NewGuid();
            return SendMessage(messageId, text, recipients);
        }

        public Guid SendAsync(string text, string recipients)
        {
            var messageId = Guid.NewGuid();

            try
            {
                Task.Run(() => this.SendMessage(messageId, text, recipients));
            }
            catch (AggregateException)
            {
                // TODO
            }

            return messageId;
        }

        private Guid SendMessage(Guid messageId, string text, string recipients)
        {
            SendMessageCommand command = new SendMessageCommand(messageId.ToString(), text, recipients, false);
            _sendMessageHandler.Handle(command);

            return messageId;
        }
    }
}
