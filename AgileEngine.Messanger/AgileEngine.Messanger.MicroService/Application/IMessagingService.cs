﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.MicroService
{
    public interface IMessagingService
    {
        Guid Send(string text, string recipients);
        Guid SendAsync(string text, string recipients);
    }
}
