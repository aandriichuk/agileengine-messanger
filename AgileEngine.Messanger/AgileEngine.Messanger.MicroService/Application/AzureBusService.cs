﻿using AgileEngine.Messanger.Infrastructure.Framework;
using AgileEngine.Messanger.Infrastructure.Framework.EventStore;
using Microsoft.ServiceBus.Messaging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.MicroService
{
    public class AzureBusService : IBusService
    {
        public IEventStore EventStore { get; private set; }

        public AzureBusService(IEventStore eventStore)
        {
            EventStore = eventStore;
        }

        public void RaiseEvent<T>(T theEvent) where T : Event
        {
            SendMessage<T>(theEvent);
            EventStore.Save(theEvent);
        }

        public void Send<T>(T command) where T : Command
        {
            SendMessage<T>(command);
        }

        private void SendMessage<T>(T message)
        {
            // Create the MessagingFactory
            MessagingFactory factory = MessagingFactory.CreateFromConnectionString(""); // TODO: add connection string

            // Create Request and Response Queue Clients
            QueueClient requestQueueClient = factory.CreateQueueClient(""); // TODO: add request queue name
            QueueClient responseQueueClient = factory.CreateQueueClient(""); // TODO: add response queue name

            // Create a session identifier for the response message
            string responseSessionId = Guid.NewGuid().ToString();

            // Create a message using text as the body.
            BrokeredMessage requestMessage = new BrokeredMessage(message);

            // Set the appropriate message properties.
            requestMessage.ReplyToSessionId = responseSessionId;

            var stopwatch = Stopwatch.StartNew();

            // Send the message on the request queue.
            requestQueueClient.Send(requestMessage);

            // Accept a message session.
            MessageSession responseSession =
                responseQueueClient.AcceptMessageSession(responseSessionId);

            // Receive the response message.
            BrokeredMessage responseMessage = responseSession.Receive();
            stopwatch.Stop();

            // Close the session, we got the message.
            responseSession.Close();

            // Deserialise the message body to echoText.
            var response = responseMessage.GetBody<T>();

            // TODO: do something with response
        }
    }
}
