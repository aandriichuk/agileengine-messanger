﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using AgileEngine.DataLayer.DataBase.Initialize;
using AgileEngine.Messanger.Infrastructure.Framework;
using AgileEngine.Messanger.CommandStack.Commands;
using AgileEngine.Messanger.CommandStack;
using AgileEngine.DataLayer.Common.Repositories;
using AgileEngine.Messanger.Infrastructure.Framework.Repositories;
using AgileEngine.Messanger.Infrastructure.Framework.EventStore;
using AgileEngine.Messanger.EventStore.SqlServer;

namespace AgileEngine.Messanger.MicroService
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            services.AddTransient<IRepository, MessageRepository>();
            services.AddTransient<IEventStore, SqlEventStore>();
            services.AddTransient<IHandleMessage<SendMessageCommand>, SendMessageCommandHandler>();
            services.AddTransient<IBusService, AzureBusService>();
            services.AddTransient<IMessagingService, MessagingService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            DatabaseSetup.Initialize();

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
        }
    }
}
