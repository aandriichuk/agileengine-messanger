﻿using AgileEngine.DataLayer.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.QueryStack
{
    public interface IDatabase
    {
        IQueryable<Message> Messages { get; }
    }
}
