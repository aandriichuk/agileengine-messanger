﻿using AgileEngine.DataLayer.Common;
using AgileEngine.DataLayer.Common.Models;
using System.Linq;

namespace AgileEngine.Messanger.QueryStack
{
    public class Database : IDatabase
    {
        private readonly MessangerDbContext context;
        public Database()
        {
            context = new MessangerDbContext();
        }

        public IQueryable<Message> Messages
        {
            get
            {
                return context.Messages;
            }
        }
    }
}
