﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AgileEngine.DataLayer.Common.Repositories;
using AgileEngine.Messanger.Infrastructure.Framework.EventStore;
using AgileEngine.Messanger.Infrastructure.Framework;
using AgileEngine.DataLayer.Common.Models;
using System.Web.Script.Serialization;

namespace AgileEngine.Messanger.EventStore.SqlServer
{
    public class SqlEventStore : IEventStore
    {
        private static readonly EventRepository EventRepository = new EventRepository();

        public IEnumerable<Event> All(string matchId)
        {
            return null; //EventRepository.All(matchId);
        }

        public void Save<T>(T theEvent) where T : Event
        {
            var loggedEvent = new LoggedEvent()
            {
                Action = theEvent.Name,
                AggregateId = theEvent.Id,
                Cargo = new JavaScriptSerializer().Serialize(theEvent)
            };

            EventRepository.Store(loggedEvent);
        }
    }
}
