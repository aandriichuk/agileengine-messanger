﻿using AgileEngine.DataLayer.Common;
using AgileEngine.DataLayer.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.EventStore.SqlServer
{
    public class EventRepository
    {
        private readonly MessangerDbContext context = new MessangerDbContext();

        public void Store(LoggedEvent eventToLog)
        {
            eventToLog.TimeStamp = DateTime.Now;
            context.Events.Add(eventToLog);
            context.SaveChanges();
        }

        public IList<LoggedEvent> All(int aggregateId)
        {
            var events = (from e in context.Events where e.AggregateId == aggregateId select e).ToList();
            return events;
        }
    }
}
