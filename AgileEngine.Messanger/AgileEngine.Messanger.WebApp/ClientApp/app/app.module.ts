import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { UniversalModule } from 'angular2-universal';
import { AppComponent } from './components/app/app.component'
import { NavMenuComponent } from './components/navmenu/navmenu.component';
import { HomeComponent } from './components/home/home.component';
import { MessangerComponent } from './components/messanger/messanger.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MessangerService } from '../app/services/app.messanger.service';

@NgModule({
    bootstrap: [ AppComponent ],
    declarations: [
        AppComponent,
        NavMenuComponent,
        MessangerComponent,
        HomeComponent
    ],
    providers: [
        MessangerService
    ],
    imports: [
        UniversalModule, // Must be first import. This automatically imports BrowserModule, HttpModule, and JsonpModule too.
        FormsModule,
        ReactiveFormsModule,
        RouterModule.forRoot([
            { path: '', redirectTo: 'home', pathMatch: 'full' },
            { path: 'home', component: HomeComponent },
            { path: 'messanger', component: MessangerComponent },
            { path: '**', redirectTo: 'home' }
        ])
    ]
})
export class AppModule {
}
