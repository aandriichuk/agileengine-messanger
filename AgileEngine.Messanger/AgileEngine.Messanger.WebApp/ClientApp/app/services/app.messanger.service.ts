import { Injectable } from '@angular/core'
import { Headers, Http, RequestOptions, URLSearchParams, ResponseContentType } from '@angular/http';
import 'rxjs/add/operator/toPromise';

@Injectable()
export class MessangerService {

    constructor(private http: Http) { }

    sendMessage(serviceUrl: string, text: string, recipients: string): Promise<any> {
        let body = JSON.stringify({ Text: text, Recipients: recipients });
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http.post(serviceUrl, body, options)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        console.error('An error occurred', error); // for demo purposes only
        return Promise.reject(error.message || error);
    }
}