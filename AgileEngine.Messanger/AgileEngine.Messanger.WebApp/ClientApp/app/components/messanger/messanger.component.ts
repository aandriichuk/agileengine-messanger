import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { MessangerService } from '../../services/app.messanger.service';

export class MessageData {
    serviceUrl: string;
    text: string;
    recipients: string;
    messageId: string;
}

@Component({
    selector: 'messanger',
    templateUrl: './messanger.component.html'
})

export class MessangerComponent {
    public data: MessageData = {
        serviceUrl: 'http://localhost:8248/api/sendmessage',
        text: 'test message',
        recipients: '12,15,16,17',
        messageId: ''
    }

    constructor(private messangerService: MessangerService) {
    }

    sendMessage(event) {
        this.messangerService.sendMessage(this.data.serviceUrl, this.data.text, this.data.recipients).then(response => {
            this.data.messageId = response;
        });
    }
}
