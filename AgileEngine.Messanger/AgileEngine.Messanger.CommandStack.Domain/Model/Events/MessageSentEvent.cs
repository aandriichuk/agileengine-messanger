﻿using AgileEngine.Messanger.CommandStack.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.CommandStack.Domain.Model.Events
{
    public class MessageSentEvent : DomainEvent
    {
        public MessageSentEvent(string messageId, string text, string recipients, bool isSent)
        {
            MessageId = messageId;
            Text = text;
            Recipients = recipients;
            IsSent = isSent;
        }

        public string MessageId { get; private set; }
        public string Text { get; private set; }
        public string Recipients { get; private set; }
        public bool IsSent { get; private set; }
    }
}
