﻿using AgileEngine.Messanger.CommandStack.Domain.Common;
using AgileEngine.Messanger.CommandStack.Domain.Model.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.DataLayer.Common.Models
{
    public class SendMessageRequest : Aggregate
    {
        public string MessageId { get; set; }
        public string Recipients { get; set; }
        public string Text { get; set; }
        public bool IsSent { get; set; }

        public void Apply(MessageSentEvent evt)
        {
            MessageId = evt.MessageId;
            Text = evt.Text;
            Recipients = evt.Recipients;
            IsSent = evt.IsSent;
        }

        public static class Factory
        {
            public static SendMessageRequest Create(string messageId, string text, string recipients, bool isSent)
            {
                var requested = new MessageSentEvent(messageId, text, recipients, isSent);
                var request = new SendMessageRequest();
                request.RaiseEvent(requested);
                return request;
            }
        }
    }
}
