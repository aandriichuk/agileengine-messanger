﻿using AgileEngine.Messanger.Infrastructure.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.CommandStack.Events
{
    public class MessageSentEvent : Event
    {
        public MessageSentEvent(Guid requestId, int id)
        {
            RequestId = requestId;
            Id = id;
            When = DateTime.Now;
        }

        public Guid RequestId { get; set; }
        public DateTime When { get; set; }
    }
}
