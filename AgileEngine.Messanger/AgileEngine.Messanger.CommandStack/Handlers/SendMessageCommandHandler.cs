﻿using AgileEngine.DataLayer.Common.Models;
using AgileEngine.DataLayer.Common.Repositories;
using AgileEngine.Messanger.CommandStack.Commands;
using AgileEngine.Messanger.CommandStack.Events;
using AgileEngine.Messanger.Infrastructure.Framework;
using AgileEngine.Messanger.Infrastructure.Framework.EventStore;
using AgileEngine.Messanger.Infrastructure.Framework.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.CommandStack
{
    public class SendMessageCommandHandler : BaseHandler, IHandleMessage<SendMessageCommand>
    {
        private readonly IRepository _repository;

        public SendMessageCommandHandler(IBusService bus, IEventStore eventStore) : base(bus, eventStore)
        {
            _repository = new MessageRepository();
        }

        public SendMessageCommandHandler(IBusService bus, IEventStore eventStore, IRepository repository) : base(bus, eventStore)
        {
            _repository = repository;
        }

        public void Handle(SendMessageCommand message)
        {
            var request = SendMessageRequest.Factory.Create(message.MessageId, message.Text, message.Recipients, message.IsSent);
            var response = _repository.CreateMessageFromRequest(request);
            if (!response.Success)
            {
                var rejected = new MessageFailedEvent(request.Id, response.Description);
                Bus.RaiseEvent(rejected);
                return;
            }

            var created = new MessageSentEvent(request.Id, response.AggregateId);
            Bus.RaiseEvent(created);
        }
    }
}
