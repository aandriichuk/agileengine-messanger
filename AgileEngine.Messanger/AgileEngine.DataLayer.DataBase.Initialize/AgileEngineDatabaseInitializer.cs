﻿using AgileEngine.DataLayer.Common;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Migrations;
using System.Linq;

namespace AgileEngine.DataLayer.DataBase.Initialize
{
    public class AgileEngineDatabaseInitializer : MigrateDatabaseToLatestVersion<MessangerDbContext, Configuration>
    {
        public override void InitializeDatabase(MessangerDbContext context)
        {
            //var database = context.Database;
            var config = (DbMigrationsConfiguration)Activator.CreateInstance<Configuration>();
            //config.TargetDatabase = new DbConnectionInfo(database.Connection.ConnectionString, "System.Data.SqlClient");
            var migrator = new DbMigrator(config);
            if (migrator.GetPendingMigrations().Any())
            {
                throw new ApplicationException($"Migrations {string.Join(",", migrator.GetPendingMigrations())} are pending. Please run them before start app");
            }
        }
    }
}
