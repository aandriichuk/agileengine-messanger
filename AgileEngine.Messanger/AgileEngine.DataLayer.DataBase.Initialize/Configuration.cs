﻿using AgileEngine.DataLayer.Common;
using System;
using System.Collections.Generic;
using System.Data.Entity.Migrations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.DataLayer.DataBase.Initialize
{
    public sealed class Configuration : DbMigrationsConfiguration<MessangerDbContext>
    {
        #region Constructor

        public Configuration()
        {
            this.AutomaticMigrationDataLossAllowed = true;
        }

        #endregion
    }
}
