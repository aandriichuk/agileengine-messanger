﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.DataLayer.DataBase.Initialize
{
    public class DatabaseSetup
    {
        public static void Initialize()
        {
            Database.SetInitializer(new AgileEngineDatabaseInitializer());
        }
    }
}
