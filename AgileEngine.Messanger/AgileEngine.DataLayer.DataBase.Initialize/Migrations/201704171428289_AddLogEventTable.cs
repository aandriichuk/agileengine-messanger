namespace AgileEngine.DataLayer.DataBase.Initialize
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLogEventTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoggedEvents",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Action = c.String(),
                        AggregateId = c.Int(nullable: false),
                        Cargo = c.String(),
                        TimeStamp = c.DateTime(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LoggedEvents");
        }
    }
}
