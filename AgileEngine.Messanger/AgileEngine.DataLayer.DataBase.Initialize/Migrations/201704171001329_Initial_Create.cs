namespace AgileEngine.DataLayer.DataBase.Initialize
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_Create : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Message",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        MessageId = c.String(maxLength: 8000, unicode: false),
                        Text = c.String(nullable: false, maxLength: 255),
                        IsSent = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.MessageId, name: "IX_Message_MessageId");
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.Message", "IX_Message_MessageId");
            DropTable("dbo.Message");
        }
    }
}
