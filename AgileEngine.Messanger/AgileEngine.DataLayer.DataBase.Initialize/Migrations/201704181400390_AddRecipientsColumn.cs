namespace AgileEngine.DataLayer.DataBase.Initialize
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddRecipientsColumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Message", "Recipients", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Message", "Recipients");
        }
    }
}
