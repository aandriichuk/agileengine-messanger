﻿namespace AgileEngine.Messanger.Infrastructure.Framework.EventStore
{
    public interface IEventStore
    {
        void Save<T>(T theEvent) where T : Event;
    }
}
