﻿using AgileEngine.Messanger.Infrastructure.Framework.EventStore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.Infrastructure.Framework
{
    public abstract class BaseHandler
    {
        public IBusService Bus { get; private set; }
        public IEventStore EventStore { get; private set; }

        protected BaseHandler(IBusService bus, IEventStore eventStore)
        {
            if (bus == null)
            {
                throw new ArgumentNullException("bus");
            }

            Bus = bus;
            EventStore = eventStore;
        }
    }
}
