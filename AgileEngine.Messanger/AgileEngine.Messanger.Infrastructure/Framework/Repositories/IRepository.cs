﻿using AgileEngine.Messanger.CommandStack.Domain.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.Infrastructure.Framework.Repositories
{
    public interface IRepository
    {
        //TEntity Get(TKey id);
        //void Save(TEntity entity);
        //void Delete(TEntity entity);

        CommandResponse CreateMessageFromRequest<T>(T item) where T : class, IAggregate;
    }
}
