﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.Infrastructure.Framework
{
    public class Message
    {
        public int Id { get; protected set; }
        public string Name { get; protected set; }
    }
}
