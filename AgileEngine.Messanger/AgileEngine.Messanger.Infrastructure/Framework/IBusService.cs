﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.Messanger.Infrastructure.Framework
{
    public interface IBusService
    {
        void Send<T>(T command) where T : Command;

        void RaiseEvent<T>(T theEvent) where T : Event;
    }
}
