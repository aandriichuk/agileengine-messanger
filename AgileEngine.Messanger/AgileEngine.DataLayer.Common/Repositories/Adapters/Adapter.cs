﻿using AgileEngine.DataLayer.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.DataLayer.Common.Repositories.Adapters
{
    public class Adapter
    {
        public static Message RequestToMessage(SendMessageRequest entity)
        {
            var message = new Message
            {
                MessageId = entity.MessageId,
                Text = entity.Text,
                Recipients = entity.Recipients,
                IsSent = entity.IsSent
            };
            return message;
        }
    }
}
