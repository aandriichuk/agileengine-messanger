﻿using AgileEngine.DataLayer.Common.Models;
using AgileEngine.DataLayer.Common.Repositories.Adapters;
using AgileEngine.Messanger.CommandStack.Domain.Common;
using AgileEngine.Messanger.Infrastructure.Framework;
using AgileEngine.Messanger.Infrastructure.Framework.Repositories;
using System;

namespace AgileEngine.DataLayer.Common.Repositories
{
    public class MessageRepository: IRepository
    {
        private readonly MessangerDbContext context;
        public MessageRepository()
        {
            context = new MessangerDbContext();
        }

        public T GetById<T>(int id) where T : IAggregate
        {
            throw new NotImplementedException();
        }

        public CommandResponse CreateMessageFromRequest<T>(T item) where T : class, IAggregate
        {
            var request = item as SendMessageRequest;
            var message = Adapter.RequestToMessage(request);

            context.Messages.Add(message); 
            var count = context.SaveChanges();

            var response = new CommandResponse(count > 0, message.Id) { RequestId = new Guid(message.MessageId) };
            return response;
        }
    }
}
