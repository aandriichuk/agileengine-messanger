﻿using AgileEngine.DataLayer.Common.Models;
using AgileEngine.DataLayer.Common.Models.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.DataLayer.Common
{
    [DbConfigurationType(typeof(AgileEngineDbConfiguration))]
    public class MessangerDbContext : DbContext
    {
        public MessangerDbContext(): base("AgileEngineDb") { }

        public DbSet<Message> Messages { get; set; }
        public DbSet<LoggedEvent> Events { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {   
            modelBuilder.Configurations.Add(new MessageMap());
        }
    }
}
