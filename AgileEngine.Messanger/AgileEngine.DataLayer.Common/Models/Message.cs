﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.DataLayer.Common.Models
{
    public class Message
    {
        public int Id { get; set; }
        public string MessageId { get; set; }
        public string Recipients { get; set; }
        public string Text { get; set; }
        public bool IsSent { get; set; }
    }
}
