﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.DataLayer.Common.Models.Mapping
{
    public class LoggedEventMap : EntityTypeConfiguration<LoggedEvent>
    {
        public LoggedEventMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Table and Column Mappings
            this.ToTable("LoggedEvent");
            this.Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
