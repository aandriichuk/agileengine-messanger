﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.DataLayer.Common.Models.Mapping
{
    public class MessageMap : EntityTypeConfiguration<Message>
    {
        public MessageMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Text).HasMaxLength(255);

            // Table and Column Mappings
            this.ToTable("Message");
            this.Property(t => t.Id).HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            this.Property(t => t.Text).HasColumnName("Text").IsRequired();
            this.Property(t => t.Recipients).HasColumnName("Recipients").IsRequired();
            this.Property(t => t.MessageId).HasColumnType("varchar").HasColumnName("MessageId").HasColumnAnnotation("Index", new IndexAnnotation(new IndexAttribute("IX_Message_MessageId")));
        }
    }
}
