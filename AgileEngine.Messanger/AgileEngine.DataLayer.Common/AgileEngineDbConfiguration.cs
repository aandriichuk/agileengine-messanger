﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AgileEngine.DataLayer.Common
{
    public class AgileEngineDbConfiguration: DbConfiguration
    {
        public AgileEngineDbConfiguration()
        {
            this.SetProviderServices(SqlProviderServices.ProviderInvariantName, SqlProviderServices.Instance);
        }

    }
}
